<%-- 
    Document   : login
    Created on : Apr 21, 2020, 10:40:43 PM
    Author     : Lazar Popadic
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    
    <body>
    <center><h1>Login</h1></center>
    <br><br><br>
     <center>
         ${message}
        <br><br>
        <form action="/fisweb/app/login" method="post">
            Email:
            <br>
            <div>
                <input type="text" id="email" name="email"/>
            </div>
            <p/>
            Password:
            <br>
            <div>
                <input type="password" id="password" name="password"/>
            </div>
            <p/>
            <input type="submit" id="Login" value="Log in"/>
            <center/>
        </form>
    </body>
</html>

