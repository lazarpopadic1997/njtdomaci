<%-- 
    Document   : home
    Created on : Apr 21, 2020, 10:34:30 PM
    Author     : Lazar Popadic
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <body>
    <center>
        <%@include file="template/header.jsp" %>
        <%@include file="template/menu.jsp" %>
        <%@include file="template/footer.jsp" %>
        </center>
    </body>
</html>
