/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package popadic.lazar.fon.fisweb.action_factory;

import popadic.lazar.fon.fisweb.action.impl.UpdateDepartmentAction;
import popadic.lazar.fon.fisweb.action.impl.DeleteDepartmentAction;
import popadic.lazar.fon.fisweb.action.impl.EditDepartmentAction;
import popadic.lazar.fon.fisweb.constants.ActionConstants;
import popadic.lazar.fon.fisweb.action.AbstractAction;
import popadic.lazar.fon.fisweb.action.impl.AddDepartmentAction;
import popadic.lazar.fon.fisweb.action.impl.AllDepartmentsAction;
import popadic.lazar.fon.fisweb.action.impl.LoginAction;
import popadic.lazar.fon.fisweb.action.impl.SaveDepartmentAction;

/**
 *
 * @author Lazar Popadic
 */
public class ActionFactory {

    public static AbstractAction createActionFactory(String actionName) {
        AbstractAction action = null;
        if (actionName.equals(ActionConstants.URL_LOGIN)) {
            action = new LoginAction();
        }
        if (actionName.equals(ActionConstants.URL_ALL_DEPARTMENTS)) {
            action = new AllDepartmentsAction();
        }
        if (actionName.equals(ActionConstants.URL_ADD_DEPARTMENT)) {
            action = new AddDepartmentAction();
        }
        if (actionName.equals(ActionConstants.URL_SAVE_DEPARTMENT)) {
            action = new SaveDepartmentAction();
        }
        if (actionName.startsWith(ActionConstants.URL_EDIT_DEPARTMENT)) {
            action = new EditDepartmentAction();
        }
        if (actionName.startsWith(ActionConstants.URL_DELETE_DEPARTMENT)) {
            action = new DeleteDepartmentAction();
        }
        if (actionName.startsWith(ActionConstants.URL_UPDATE_DEPARTMENT)) {
            action = new UpdateDepartmentAction();
        }
        return action;
    }
}
